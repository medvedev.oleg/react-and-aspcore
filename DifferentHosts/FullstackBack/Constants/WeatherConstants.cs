﻿using FullstackBack.Models;

namespace FullstackBack.Constants
{
    public class WeatherConstants
    {
        public static string[] Temperature = ["-10", "-9", "-8", "-7", "-6", "-5", "-4", "-3", "-2", "-1", "0", "+1", "+2", "+3", "+4", "+5", "+6", "+7", "+8", "+9", "+10",];

        public static string[] City = ["Москва", "Санкт-Петербург", "Новосибирск", "Екатеринбург", "Казань", "Нижний Новгород", "Красноярск", "Челябинск"];

        public static WeatherTypeItem[] WeatherTypes = [
            new WeatherTypeItem { Id = 1, Value = "Солнечно" },
            new WeatherTypeItem { Id = 2, Value = "Облачно" },
            new WeatherTypeItem { Id = 3, Value = "Дождь" },
            new WeatherTypeItem { Id = 4, Value = "Снег" },
        ];
    }

}
