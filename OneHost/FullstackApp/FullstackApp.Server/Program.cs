
namespace FullstackApp.Server
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();
            builder.Services.AddCors();


            var app = builder.Build();

            app.UseDefaultFiles();
            app.UseStaticFiles();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();


            //app.UseSpa(spa =>
            //{
            //    spa.Options.SourcePath = "wwwroot";
            //    spaBuilder.UseProxyToSpaDevelopmentServer(baseUri: "https://localhost:5001");
            //});

            app.UseAuthorization();
            //app.UseCors(builder => builder.WithOrigins("https://localhost:5173", "http://localhost:5173"));
            app.UseCors(builder => builder.WithOrigins("*"));

            app.MapControllers();

            app.MapFallbackToFile("/index.html");

            app.Run();
        }
    }
}
