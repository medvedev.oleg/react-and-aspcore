﻿using static FullstackApp.Constants.WeatherConstants;

namespace FullstackApp.Models
{
    public class Weather
    {
        public string City { get; set; }
        public WeatherTypeItem WeatherType { get; set; }
        public string Temperature { get; set; }
    } 
}
